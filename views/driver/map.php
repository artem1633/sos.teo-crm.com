<?php


\app\assets\MapAsset::register($this);

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Машины</h4>
            </div>
            <div class="panel-body">
                <div id="drivers-map" style="width: 100%; height: 75vh;">

                </div>
            </div>
        </div>
    </div>
</div>
