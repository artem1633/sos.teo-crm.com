<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */
?>
<div class="driver-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'phone',
            'mark',
            'model',
            'color',
            'number',
            'city',
            'sos',
            'position_x',
            'position_y',
            'datetime_position',
            'access',
            'token',
            'push_token',
            'created_at',
        ],
    ]) ?>

</div>
