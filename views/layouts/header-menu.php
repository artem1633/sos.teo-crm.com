<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Онлайн карта', 'icon' => 'fa  fa-map-o', 'url' => ['/driver/map'],],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'],],
                    ['label' => 'Города', 'icon' => 'fa  fa-user-o', 'url' => ['/city'],],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
