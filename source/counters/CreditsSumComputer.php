<?php

namespace app\source\counters;

/**
 * Class CreditsSumComputer
 * @package app\source\counters
 * Считает сумму выплат по кредиту по конкретному авто ($car)
 *
 * @property \app\models\Cars $car
 */
class CreditsSumComputer implements IComputer
{
    public $car;

    /**
     * CreditsSumComputer constructor.
     * @param $car
     */
    function __construct($car)
    {
        $this->car = $car;
    }


    /**
     * Считает сумму выплат по кредиту
     * @return int
     */
    public function compute()
    {
        $creditSum = 0;
        $credit = $this->car->credit;

        if($credit == null)
            return 0;

        $payments = $credit->creditPayments;

        foreach($payments as $payment) {
            $creditSum .= $payment->sum;
        }

        return $creditSum;
    }
}