<?php

namespace app\source\counters;

use app\models\books\AdditionalServices;

/**
 * Class DebitsComputer
 * @package app\source\counters
 * Считает доход от аренды, от дополнительных услуг из всех броней авто ($car)
 *
 * @property \app\models\Cars $car
 */
class DebitsComputer implements IComputer
{
    public $car;

    function __construct($car)
    {
        $this->car = $car;
    }

    /**
     * Считает доход от авто за
     * все время его существования в системе
     * @inheritdoc
     * @return float|int
     */
    public function compute()
    {
        $debit = 0;

        $employmets = $this->car->carRentEmployments;

        foreach ($employmets as $employment)
        {
            $debit += $employment->rent_price_paid;

            // TODO: НИЖЕ ПРИВИДЕН НЕ ВЕРНЫЙ АЛГОРИТМ ПОДСЧЕТА ДОХОДА, ТАК КАК ЦЕНА НА УСЛУГИ МОЖЕТ МЕНЯТЬСЯ СО ВРЕМЕНЕМ ПО РАЗНЫМ
            if($employment->additional_services != null && $employment->additional_services != '')
            {
                $additionalServicesPks = explode(',', $employment->additional_services);
                $additionalServices = AdditionalServices::findAll($additionalServicesPks);

                foreach ($additionalServices as $service)
                {
                    $debit += $service->price * $employment->getDaysCount();
                }
            }
        }

        return $debit;
    }

}