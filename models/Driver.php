<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "driver".
 *
 * @property int $id
 * @property string $phone Телефон
 * @property string $mark Марка
 * @property string $model Модель
 * @property string $color Цвет
 * @property string $number Номер
 * @property string $city_id Город
 * @property int $sos SOS
 * @property string $position_x X
 * @property string $position_y Y
 * @property string $datetime_position Дата и время позиции
 * @property int $access Доступ
 * @property string $token Токен
 * @property string $push_token Push токен
 * @property string $created_at Дата и время
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sos', 'access'], 'integer'],
            [['datetime_position', 'created_at'], 'safe'],
            [['phone', 'mark', 'model', 'color', 'number', 'city_id', 'position_x', 'position_y', 'token', 'push_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'mark' => 'Марка',
            'model' => 'Модель',
            'color' => 'Цвет',
            'number' => 'Номер',
            'city_id' => 'Город',
            'sos' => 'SOS',
            'position_x' => 'X',
            'position_y' => 'Y',
            'datetime_position' => 'Дата и время позиции',
            'access' => 'Доступ',
            'token' => 'Токен',
            'push_token' => 'Push токен',
            'created_at' => 'Дата и время',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->token = Yii::$app->security->generateRandomString();
        }

        return parent::beforeSave($insert);
    }

    public function sendNotification($title, $text, $additional = [])
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $request_body = [
            'to' => $this->push_token,
//            'notification' => [
//                'title' => $title,
//                'body' => $text,
//            ],
            'data' => $additional,
        ];

        Yii::warning($request_body, 'push params');

        $fields = json_encode($request_body);

        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=AIzaSyBWcxb8VEAnwhfUAxTsDkdqY0IPza8X-uo',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);

        Yii::warning($response, 'push response');

        var_dump($request_body);

        var_dump($response);

    }
}
