function init()
{
    myMap = new ymaps.Map('drivers-map', {
        // center: center_map,
        center: [55.76, 37.64],
        zoom: 12,
    }, {
        buttonMaxWidth: 300
    });

    objects = [];

    updateDrivers(myMap);

    setInterval(function(){
        updateDrivers(myMap);
    }, 1000);
}

var object = null;

function getBalloonContent(car){
    var balloon = '<p><b>'+car.mark+' '+car.model+'</b></p>'+
        '<p>'+car.number+'</p>' +
        '<p>Цвет: '+car.color+'</p>';

    if(car.sos == 1){
        balloon += '<p><button class="btn btn-danger btn-sm" onclick="'+
            '$.get(\'/driver/cancel-sos?id=\'+'+car.id+', function(){ })'
            +'">Отменить СОС</button></p>';
    }

    return balloon;
}

function updateDrivers(map)
{
    $.get('/driver/json', function(response){
        for(var i = 0; i <= response.length; i++)
        {
            var car = response[i];

            if(car == null){
                continue;
            }

            var balloon = getBalloonContent(car);

            if(objects[car.id] == undefined){

                var myPlacemark = new ymaps.Placemark([car.position_x, car.position_y], {
                    balloonContent: balloon,
                }, {
                    // geometry: {
                    //     type: "Point",
                    //     coordinates: [car.position_x, car.position_y],
                    // },
                    preset: car.sos === 1 ? 'islands#redGlyphIcon' : 'islands#glyphIcon',
                });

                map.geoObjects.add(myPlacemark);

                objects[car.id] = myPlacemark;
            } else {
                window.object = objects[car.id];
                objects[car.id].geometry.setCoordinates([car.position_x, car.position_y]);
                objects[car.id].options.set('preset', car.sos === 1 ? 'islands#redGlyphIcon' : 'islands#glyphIcon');
                objects[car.id].properties.set('balloonContent', balloon);
            }
        }
    });
}

ymaps.ready(init);