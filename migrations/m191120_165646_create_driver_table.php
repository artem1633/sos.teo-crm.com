<?php

use yii\db\Migration;

/**
 * Handles the creation of table `driver`.
 */
class m191120_165646_create_driver_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('driver', [
            'id' => $this->primaryKey(),
            'phone' => $this->string()->comment('Телефон'),
            'mark' => $this->string()->comment('Марка'),
            'model' => $this->string()->comment('Модель'),
            'color' => $this->string()->comment('Цвет'),
            'number' => $this->string()->comment('Номер'),
            'city_id' => $this->integer()->comment('Город'),
            'sos' => $this->boolean()->defaultValue(false)->comment('SOS'),
            'position_x' => $this->string()->comment('X'),
            'position_y' => $this->string()->comment('Y'),
            'datetime_position' => $this->dateTime()->comment('Дата и время позиции'),
            'access' => $this->boolean()->defaultValue(false)->comment('Доступ'),
            'token' => $this->string()->comment('Токен'),
            'push_token' => $this->string()->comment('Push токен'),
            'created_at' => $this->dateTime()->comment('Дата и время'),
        ]);

        $this->createIndex(
            'idx-driver-city_id',
            'driver',
            'city_id'
        );

        $this->addForeignKey(
            'fk-driver-city_id',
            'driver',
            'city_id',
            'city',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-driver-city_id',
            'driver'
        );

        $this->dropIndex(
            'idx-driver-city_id',
            'driver'
        );

        $this->dropTable('driver');
    }
}
