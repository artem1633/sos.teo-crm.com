<?php

namespace app\modules\api\controllers;

use app\models\City;
use app\models\Driver;
use Yii;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UserController extends Controller
{
    /**
     * @var MobileUser
     */
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * POST
     */
    public function actionRegister()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new RegisterModel();

        if($model->load($data, 'model')) {

            $user = Driver::find()->where(['phone' => $model->login])->one();

            if($user != null){
                return ['result' => $user->token];
            }

            $token = $model->register();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    public function actionCities()
    {
        $models = City::find()->all();

        return $models;
    }

    /**
     * POST
     */
    public function actionSetFcmToken()
    {
        $token = $_POST['fcm_token'];

        $this->user->push_token = $token;
        $result = $this->user->save(false);

        return ['result' => $result];
    }

    public function actionUpdateCoords($x, $y)
    {
        $this->user->position_x = $x;
        $this->user->position_y = $y;

        $this->user->save(false);
    }

    public function actionSosOn()
    {
        $this->user->sos = 1;

        $drivers = Driver::find()->where(['!=', 'id', $this->user->id])->andWhere(['city_id' => $this->user->city_id])->all();

        $driverNumber = $this->user->number;
        foreach ($drivers as $driver){
            $driver->sendNotification('Сигнал SOS!', "Водитель под номерным знаком {$driverNumber} подал сигнал сос", [
                'x' => $this->user->position_x,
                'y' => $this->user->position_y,
                'phone' => $this->user->phone,
                'mark' => $this->user->mark,
                'model' => $this->user->model,
                'color' => $this->user->color,
                'number' => $this->user->number,
                'city' => $this->user->city_id,
                'type' => 'on'
            ]);
        }

        $this->user->save(false);
    }


    public function actionSosOff()
    {
        $this->user->sos = 0;
        $this->user->save(false);


        $drivers = Driver::find()->where(['!=', 'id', $this->user->id])->andWhere(['city_id' => $this->user->city_id])->all();

        $driverNumber = $this->user->number;
        foreach ($drivers as $driver){
            $driver->sendNotification('Отмена сигнала SOS!', "Водитель под номерным знаком {$driverNumber} отменил сигнал сос", [
                'x' => $this->user->position_x,
                'y' => $this->user->position_y,
                'phone' => $this->user->phone,
                'mark' => $this->user->mark,
                'model' => $this->user->model,
                'color' => $this->user->color,
                'number' => $this->user->number,
                'city' => $this->user->city_id,
                'type' => 'off'
            ]);
        }

    }

//    /**
//     * POST
//     */
//    public function actionGetToken()
//    {
//        $request = Yii::$app->request;
//        $data = ['model' => $request->post()];
//        $model = new Login();
//
//        if($model->load($data, 'model')) {
//
//            $token = $model->login();
//
//            if($token == null){
//                return $model->errors;
//            }
//
//            return ['result' => $token];
//        }
//
//        return ['result' => false];
//    }

    public function beforeAction($action)
    {
        if(in_array($action->id, ['register', 'cities']) == false){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = Yii::$app->request;
            $token = null;

            if($request->isPost){
                $token = isset($_POST['token']) ? $_POST['token'] : null;
            } else if ($request->isGet){
                $token = isset($_GET['token']) ? $_GET['token'] : null;
            }

            if($token){
                $this->user = Driver::find()->where(['token' => $token])->one();
            }

            if($this->user == null){
                throw new ForbiddenHttpException();
            }
        }

        return parent::beforeAction($action);
    }
}