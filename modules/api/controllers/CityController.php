<?php

namespace app\modules\api\controllers;

use app\models\Driver;
use app\models\User;
use Yii;
use app\modules\api\models\Login;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CityController extends Controller
{
    /**
     * @var MobileUser
     */
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return [];
    }


    public function beforeAction($action)
    {
        if(in_array($action->id, ['change-password', 'set-fcm-token'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = Yii::$app->request;
            $token = null;

            if($request->isPost){
                $token = isset($_POST['token']) ? $_POST['token'] : null;
            } else if ($request->isGet){
                $token = isset($_GET['token']) ? $_GET['token'] : null;
            }

            if($token){
                $this->user = Driver::find()->where(['token' => $token])->one();
            }

            if($this->user == null){
                throw new ForbiddenHttpException();
            }
        }

        return parent::beforeAction($action);
    }
}