<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Документация</title>
</head>
<body>

<h2>Регистрация</h2>
<p>[POST]</p>
<a href="">user/register</a>
<p><b>login</b> — Логин</p>
<p><b>mark</b> — Марка</p>
<p><b>model</b> — Модель</p>
<p><b>color</b> — Цвет</p>
<p><b>number</b> — Номер</p>
<p><b>city</b> — Город <b>Integer</b> (Город брать из метода user/cities)</p>
<p><b>code</b> — Код</p>

<h2>Установить fcm Token</h2>
<p>[POST]</p>
<a href="">user/set-fcm-token</a>
<p><b>fcm_token</b> — FMC Токен</p>

<h2>Обновить координаты</h2>
<p>[GET]</p>
<a href="">user/update-coords</a>
<p><b>x</b> — X</p>
<p><b>y</b> — Y</p>

<h2>Включить режим SOS</h2>
<p>[GET]</p>
<a href="">user/sos-on</a>

<h2>Выключить режим SOS</h2>
<p>[GET]</p>
<a href="">user/sos-off</a>

<h2>Получить список городов</h2>
<p>[GET]</p>
<a href="">user/cities</a>

</body>
</html>