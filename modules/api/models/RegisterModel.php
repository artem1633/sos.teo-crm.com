<?php

namespace app\modules\api\models;

use app\models\Driver;
use app\models\MobileUser;
use app\models\MUserWallet;
use Yii;
use yii\base\Model;

/**
 * Class RegisterModel
 * @package app\modules\api\models
 */
class RegisterModel extends Model
{
    /**
     * @var string
     */
    public $login;

    public $code;

    public $mark;

    public $model;

    public $color;

    public $number;

    public $city;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'code'], 'required'],
            ['code', function(){
                if($this->code != 3362){
                    $this->addError('code', 'Код не верный');
                    return false;
                }
            }],
            [['login', 'code', 'mark', 'model', 'color', 'number', 'city'], 'string'],
            [['city'], 'integer'],
            ['login', 'unique', 'targetClass' => Driver::className(), 'targetAttribute' => 'phone'],
        ];
    }

    /**
     * @return string
     */
    public function register()
    {
        if($this->validate()){
            $user = new Driver([
                'phone' => $this->login,
                'mark' => $this->mark,
                'model' => $this->model,
                'color' => $this->color,
                'number' => $this->number,
                'city_id' => $this->city,
            ]);
            $user->save(false);

            return $user->token;
        }

        return null;
    }
}